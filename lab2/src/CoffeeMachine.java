import javax.annotation.processing.SupportedSourceVersion;
import java.util.Scanner;
public class CoffeeMachine {
    public static void main(String[] args) {
        Scanner scn = new Scanner(System.in);
        System.out.println("Стаканчик: \n[1 - маленький]\n[2 - средний]\n[3 - большой]");
        byte glass = scn.nextByte();
        int sum = 0;
        while (glass < 1 || glass > 3){
            System.out.println("Стаканчик: \n[1 - маленький]\n[2 - средний]\n[3 - большой]");
            glass = scn.nextByte();
        }
        String check1, check2, check3, check4, check5, check6;
        int pr1 = 0, pr2 = 0, pr3 = 0, pr4 = 0, pr5 = 0, pr6 = 0;
        check1 = "";
        check2 = "";
        check3 = "";
        check4 = "";
        check5 = "";
        check6 = "";
        switch (glass){
            case 1: {
                pr1 = 10;
                check1 = "Меленький  - " + pr1 + "\u20BD";
                System.out.println(check1);
                sum += 10;
                break;
            }
            case 2: {
                pr1 = 20;
                check1 = "Средний  - " + pr1 + "\u20BD";
                System.out.println(check1);
                sum += 20;
                break;
            }
            case 3: {
                pr1 = 30;
                check1 = "Большой  - " + pr1 + "\u20BD";
                System.out.println(check1);
                sum += 30;
                break;
            }
            default: System.out.println("Как вы сюда попали");
        }
        System.out.println("Кофе: [1 - Латте]\n[2 - Американо]\n[3 - Русиано]\n[4 - Эспрессо]");
        byte CoffeeType = scn.nextByte();
        while (CoffeeType < 1 || CoffeeType > 4)
        {
            System.out.println("Кофе: \n[1 - Латте]\n[2 - Американо]\n[3 - Русиано]\n[4 - Эспрессо]");
            CoffeeType = scn.nextByte();
        }
        switch (CoffeeType){
            case 1: {
                pr2 = 11;
                check2 = "Латте - " + pr2 + "\u20BD";
                System.out.println(check2);
                sum += 11;
                break;
            }
            case 2: {
                pr2 = 15;
                check2 = "Американо - " + pr2 + "\u20BD";
                System.out.println(check2);
                sum += 15;
                break;
            }
            case 3: {
                pr2 = 30;
                check2 = "Русиано - " + pr2 + "\u20BD";
                System.out.println(check2);
                sum += 30;
                break;
            }
            case 4: {
                pr2 = 17;
                check2 = "Эспрессо - " + pr2 + "\u20BD";
                System.out.println(check2);
                sum += 17;
                break;
            }
        }
        System.out.println("Добавлять молоко?(д/н)");
        char milk = scn.next().charAt(0);
        switch (milk){
            case 'д': {
                pr3 = 10;
                check3 = "+молоко - " + pr3 + "\u20BD";
                System.out.println(check3);
                sum += 10;
                break;
            }
            case 'н': {
                pr3 = 0;
                check3 = "-молоко - " + pr3 + "\u20BD";
                System.out.println(check3);
                break;
            }
            default: {
                pr3 = 15;
                check3 = "+молоко - " + pr3 + "\u20BD";
                System.out.println(check3);
                sum += 15;
                break;
            }
        }
        float SugarCount;
        System.out.println("Количество сахара");
        SugarCount = scn.nextFloat();
        while (SugarCount < 0 || SugarCount > 6)
        {
            System.out.println("Слишком много/отрицательное количетсов сахара\nКоличество сахара");
            SugarCount = scn.nextInt();
        }
        sum += SugarCount * 2;
        check4 = SugarCount + " ложек сахара: " + (SugarCount * 2) + "\u20BD";

        System.out.println(check4);
        byte topping;
        System.out.println("Топпинг: \n[1 - сгущённое молоко]\n[2 - сиропчик]\n[3 - экстракт ванили]\n[4 - мороженное]" +
                "\n[5 - не добавлять]");
        topping = scn.nextByte();
        while (topping < 1 || topping > 5){
            System.out.println("Топпинг: \n[1 - сгущённое молоко]\n[2 - сиропчик]\n[3 - экстракт ванили]\n[4 - мороженное]" +
                    "\n[5 - не добавлять]");
            topping = scn.nextByte();
        }
        switch (topping){
            case 1:{
                pr4 = 7;
                check5 = "+сгущённое молоко - " + pr4 + "\u20BD";
                System.out.println(check5);
                sum += 7;
                break;
            }
            case 2:{
                pr4 = 10;
                check5 = "+сиропчик - " + pr4 + "\u20BD";
                System.out.println(check5);
                sum += 10;
                break;
            }
            case 3:{
                pr4 = 11;
                check5 = "+экстракт ванили - " + pr4 + "\u20BD";
                System.out.println(check5);
                sum += 11;
                break;
            }
            case 4:{
                pr4 = 15;
                check5 = "+мороженное - " + pr4 + "\u20BD";
                System.out.println(check5);
                sum += 15;
                break;
            }
            case 5:{
                pr4 = 0;
                check5 = "без топпинга - " + pr4 + "\u20BD";
                System.out.println(check5);
                break;
            }
        }
        System.out.println("Добавляем алкоголь?[д/н]");
        char alc = scn.next().charAt(0);
        switch (alc)
        {
            case 'д':{
                System.out.println("Подвердите что вам есть 18");
                char acceptance = scn.next().charAt(0);
                if (acceptance == 'д'){
                    System.out.println("Подтверждено");
                    byte alcohol;
                    System.out.println("Алкоголь: \n[1 - виски]\n[2 - коньяк]\n[3 - ликёр]\n[4 - самогон]");
                    alcohol = scn.nextByte();
                    while (alcohol < 1 || alcohol > 4){
                        System.out.println("Алкоголь: \n[1 - виски]\n[2 - коньяк]\n[3 - ликёр]\n[4 - самогон]");
                        alcohol = scn.nextByte();
                    }
                    switch (alcohol){
                        case 1:{
                            pr5 = 15;
                            check6 = "+виски - " + pr5 + "\u20BD";
                            System.out.println(check6);
                            sum += 15;
                            break;
                        }
                        case 2:{
                            pr5 = 13;
                            check6 = "+коньяк - " + pr5 + "\u20BD";
                            System.out.println(check6);
                            sum += 13;
                            break;
                        }
                        case 3:{
                            pr5 = 17;
                            check6 = "+ликёр - " + pr5 + "\u20BD";
                            System.out.println(check6);
                            sum += 17;
                            break;
                        }
                        case 4:{
                            pr5 = 1;
                            check6 = "+самогон - " + pr5 + "\u20BD";
                            System.out.println(check6);
                            sum += 1;
                            break;
                        }
                    }
                }
                else {
                    System.out.println("Вы не подтвердили свой возраст\nВызываем полицию");
                    System.exit(0);
                }
                break;
            }
            case 'н':{
                pr5 = 0;
                check6 = "-алкоголь - " + pr5 + "\u20BD";
                System.out.println("Не добавляем");
                break;
            }
            default:{
                pr5 = 0;
                check6 = "-алкоголь - " + pr5 + "\u20BD";
                System.out.println("Не добавляем");
                break;
            }
        }
        double totalsum = sum + sum * 0.2;
        System.out.println("С вас: " + sum + "\nСумма с учётом НДС(20%): " + totalsum + "\u20BD");
        System.out.println("Вставьте купюру в купюроприёмник: ");
        float payment = scn.nextFloat();
        while (payment < (sum + sum * 0.2)){
            System.out.println("Здесь недостаточно денях");
            payment = scn.nextFloat();
        }
        if (payment > (sum + sum * 0.2))
            System.out.println("Ваша сдача: " + (payment - totalsum) + "\u20BD");
        System.out.println("Оставляем чаевые? ");
        float tips = 0;
        char tip = scn.next().charAt(0);
        switch (tip){
            case 'д':{
                System.out.println("Сколько? ");
                tips = scn.nextFloat();
                while (tips <= 0){
                    System.out.println("Неужели обслуживание настолько плохое что вы оставляете чаевые меньше 0?(");
                    System.out.println("Может, оставите больше?");
                    tips = scn.nextFloat();
                }
                break;
            }
            case 'н':{
                tips = 0;
                System.out.println("Хорошо, мы запомним это");
                break;
            }
            default:{
                tips = 100;
                System.out.println("Непонятно что вы ввели.\nОставляете чаевые в размере 100\"\\u20BD\"");
                break;
            }
        }
        System.out.println();
        System.out.println("Чек:\nСтакан " + check1 + "\nКофе " + check2 + "\n" + check3 + "\n" + check4 +
                "\n" + check5 + "\n" + check6 + "\nИтоговая сумма: " + totalsum + "\u20BD" + "\nЧаевые: " + tips + "\u20BD");
    }
}
