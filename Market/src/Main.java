import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int id = 0;
        List<Market> markt = new ArrayList<>();
        while (true){
            System.out.println("1 - добавить товар; 2 - редактировать товар; 3 - удалить товар; 4 - просмотреть товары");
            int choose1 = scan.nextInt();
            switch (choose1) {
                case 1: {
                    id++;
                    System.out.println("Товар: \n1 - Неопределённый\n2 - Акустика\n3 - Предварительный усилитель");
                    int choose = scan.nextInt();
                    switch (choose) {
                        case 1: {
                            System.out.println("Добавление товара");
                            markt.add(new Market(inputZnach("Name: "), Float.valueOf(inputZnach("Price: ")), Integer.valueOf(inputZnach("Power(w): "))));
                            break;
                        }
                        case 2: {
                            System.out.println("Добавление товара");
                            markt.add(new Acoustic(inputZnach("Name: "), Float.valueOf(inputZnach("Price: ")), Integer.valueOf(inputZnach("Power(w): ")), Float.valueOf(inputZnach("Impedance(Om): ")),
                                    Integer.valueOf(inputZnach("Drivers(mm): "))));
                            break;
                        }
                        case 3: {
                            System.out.println("Добавление товара");
                            markt.add(new Preamplifier(inputZnach("Name: "), Float.valueOf(inputZnach("Price: ")), Integer.valueOf(inputZnach("Power(w): ")), inputZnach("DAC(name): "), inputZnach("OA(name): ")));
                            break;
                        }
                    }
                    break;
                }
                case 2:{
                    System.out.println("Товар для редактирования: ");
                    int good = scan.nextInt();
                    markt.get(good).printer();
                    if (markt.get(good) instanceof Acoustic){
                        System.out.println("1 - редактирование названия\n2 - редактирование цены\n3 - редактирование мощности\n4 - редактирование сопротивления\n5 - редактирование драйверов");
                        int edit = scan.nextInt();
                        switch (edit){
                            case 1:{
                                System.out.println("Название");
                                String nameEdit = scan.next();
                                markt.get(good).name = nameEdit;
                                break;
                            }
                            case 2:{
                                System.out.println("Цена");
                                float priceEdit = scan.nextFloat();
                                markt.get(good).price = priceEdit;
                                break;
                            }
                            case 3:{
                                System.out.println("Мощность");
                                int powerEdit = scan.nextInt();
                                markt.get(good).power = powerEdit;
                                break;
                            }
                            case 4:{
                                System.out.println("Сопротивление");
                                float impedanceEdit = scan.nextFloat();
                                ((Acoustic) markt.get(good)).impedance =impedanceEdit;
                                break;
                            }
                            case 5:{
                                System.out.println("Драйвер");
                                int driverEdit = scan.nextInt();
                                ((Acoustic) markt.get(good)).drivers = driverEdit;
                                break;
                            }
                            default:{
                                System.out.println("Неверный ввод");
                                break;
                            }
                        }
                    }
                    else if (markt.get(good) instanceof Preamplifier){
                        System.out.println("1 - редактирование названия\n2 - редактирование цены\n3 - редактирование мощности\n4 - редактирование ЦАПа\n5 - редактирование ОУ");
                        int edit = scan.nextInt();
                        switch (edit){
                            case 1:{
                                System.out.println("Название");
                                String nameEdit = scan.next();
                                markt.get(good).name = nameEdit;
                                break;
                            }
                            case 2:{
                                System.out.println("Цена");
                                float priceEdit = scan.nextFloat();
                                markt.get(good).price = priceEdit;
                                break;
                            }
                            case 3:{
                                System.out.println("Мощность");
                                int powerEdit = scan.nextInt();
                                markt.get(good).power = powerEdit;
                                break;
                            }
                            case 4:{
                                System.out.println("ЦАП");
                                String DACEdit = scan.next();
                                ((Preamplifier) markt.get(good)).DAC = DACEdit;
                                break;
                            }
                            case 5:{
                                System.out.println("ОУ");
                                String OAEdit = scan.next();
                                ((Preamplifier) markt.get(good)).OA = OAEdit;
                            }
                        }
                    }
                    else {
                        System.out.println("1 - редактирование названия\n2 - редактирование цены\n3 - редактирование мощности");
                        int edit = scan.nextInt();
                        switch (edit){
                            case 1:{
                                System.out.println("Название");
                                String nameEdit = scan.next();
                                markt.get(good).name = nameEdit;
                                break;
                            }
                            case 2:{
                                System.out.println("Цена");
                                float priceEdit = scan.nextFloat();
                                markt.get(good).price = priceEdit;
                                break;
                            }
                            case 3:{
                                System.out.println("Мощность");
                                int powerEdit = scan.nextInt();
                                markt.get(good).power = powerEdit;
                                break;
                            }
                            default:{
                                System.out.println("Неверный ввод");
                                break;
                            }
                        }
                    }
                    break;
                }
                case 3:{
                    System.out.println("Товар для удаления");
                    int removeGood = scan.nextInt();
                    markt.remove(removeGood);
                    break;
                }
                case 4: {
                    for (Market m : markt) {
                        if (m instanceof Acoustic) {
                            System.out.println("\nКласс: акустика");
                            m.printer();
                        }
                        else if (m instanceof Preamplifier){
                            System.out.println("\nКласс: предварительынй усилитель");
                            m.printer();
                        }
                        else{
                            System.out.println("Класс: неорпделённый");
                            m.printer();
                        }
                    }
                    break;
                }
            }
        }
    }

    public static String inputZnach(String type){
        Scanner scanner = new Scanner(System.in);
        System.out.println(type);
        return scanner.next();
    }
}
