public class Preamplifier extends Market{
    public String DAC;
    public String OA;

    public Preamplifier(String name, float price, int power, String DAC, String OA){
        super(name, price, power);
        this.DAC = DAC;
        this.OA = OA;
    }

    @Override
    public void printer() {
        System.out.printf("\nName: %s\nPrice: %f\nPower(Vt): %d\nDAC: %s\nOA: %s:\n", name, price, power, DAC, OA);
    }
}
