public class Acoustic extends Market{
    public float impedance;
    public int drivers;

    public Acoustic(String name, float price, int power, float impedance, int drivers){
        super(name, price, power);
        this.impedance = impedance;
        this.drivers = drivers;
    }

    @Override
    public void printer() {
        System.out.printf("\nName: %s\nPrice: %f\nPower(Vt): %d\nImpedance(Om): %f\nDrivers(mm): %d:\n", name, price, power, impedance, drivers);
    }
}
