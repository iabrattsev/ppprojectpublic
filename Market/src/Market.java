public class Market {
    public String name;
    public float price;
    public int power;

    public Market(String name, float price, int power){
        this.name = name;
        this.price = price;
        this.power = power;
    }
    public void printer(){
        System.out.printf("\nName: %s\nPrice: %f\nPower(Vt): %d\n", name, price, power);
    }
}
