import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        List<Library> lib = new ArrayList<>();
        Scanner scn = new Scanner(System.in);
        int count = 0;
        System.out.println("Количество книг:");
        count = scn.nextInt();
        for (int i = 0; i < count; i++) {
            lib.add(new Library(inputZnach("Автор: "), inputZnach("Жанр: "), Integer.valueOf(inputZnach("Кол-во страниц: "))));
        }
        while (true) {
            System.out.println("1 - добавление книг\n2 - удаление книг\n3 - просмотр книг\n4 - поиск книг");
            byte select = scn.nextByte();
            switch (select) {
                case 1: {
                    System.out.println("Добавление книг");
                    //int count = 0;
                    System.out.println("Количество книг:");
                    count = scn.nextInt();
                    for (int i = 0; i < count; i++) {
                        lib.add(new Library(inputZnach("Автор: "), inputZnach("Жанр: "), Integer.valueOf(inputZnach("Кол-во страниц: "))));
                    }
                    break;
                }
                case 2:{
                    System.out.println("Книга для удаления:");
                    int removeBook = scn.nextInt();
                    lib.remove(removeBook);
                    break;
                }
                case 3:{
                    for (Library lb : lib){
                        lb.printer();
                    }
                    break;
                }
                case 4:{
                    System.out.println("Поиск по:\n1 - автор\n2 - жанр\n3 - кол-во страниц");
                    byte choice = scn.nextByte();
                    switch (choice){
                        case 1:{
                            System.out.println("Поиск по автору\nАвтор: ");
                            String author = scn.next();
                            for (Library lb : lib){
                                if (lb.Author.equals(author)){
                                    lb.printer();
                                }
                            }
                            break;
                        }
                        case 2:{
                            System.out.println("Поиск по жанру\nЖанр: ");
                            String genre = scn.next();
                            for (Library lb : lib){
                                if (lb.Genre.equals(genre)){
                                    lb.printer();
                                }
                            }
                            break;
                        }
                        case 3:{
                            System.out.println("Поиск по кол-ву стр\nКол-во стр: ");
                            int pageCount = scn.nextInt();
                            for (Library lb : lib){
                                if (lb.PageCount == pageCount){
                                    lb.printer();
                                }
                            }
                            break;
                        }
                        default:{
                            break;
                        }
                    }
                    break;
                }
                default:{
                    break;
                }
            }
        }
    }

    public static String inputZnach(String type){
        Scanner scanner = new Scanner(System.in);
        System.out.println(type);
        return scanner.next();
    }
}
