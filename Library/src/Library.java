public class Library {
    public String Author;
    public String Genre;
    public int PageCount;

    public Library(String Author, String Genre, int PageCount){
        this.Author = Author;
        this.Genre = Genre;
        this.PageCount = PageCount;
    }

    public void printer(){
        System.out.printf("Автор: %s\nЖанр: %s\nКол-во страниц: %d\n", Author, Genre, PageCount);
    }
}
