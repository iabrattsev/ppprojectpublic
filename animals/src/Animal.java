public class Animal {
    String name;
    int age;
    String color;

    public Animal(String name, int age, String color){
        this.color = color;
        this.age = age;
        this.name = name;
    }
    public void printer(){
        System.out.printf("\nName: %s\nAge: %d\nColor: %s", name, age, color);
    }

}
