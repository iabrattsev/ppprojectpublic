public class Hamster extends Animal{
    float Hair;
    String name;
    public Hamster(String name, int age, String color, float hair){
        super(name, age, color);
        this.Hair = hair;
        this.name = name + "ham";
        super.name = name + "Anim";
    }

    @Override
    public void printer() {
        System.out.printf("\nName: %s\nAge: %d\nColor: %s\nHair: %f", name, age, color, Hair);
        //super.printer();
    }
}
