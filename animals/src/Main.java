import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        Animal anim = new Animal("Животное", 12, "Зелёный");
        anim.printer();
        Turtle turt = new Turtle("Черепаха", 15, "Чёрный", true);
        turt.printer();
        Hamster ham = new Hamster("Артём", 17, "Седой", 40);
        ham.printer();
        Animal a = new Turtle("Черепаха", 15, "Чёрный", true);
        List<Animal> animals = new ArrayList<>();
        animals.add(new Animal("Животное", 12, "Зелёный"));
        animals.add(new Turtle("Черепаха", 15, "Чёрный", true));
        animals.add(new Hamster("Артём", 17, "Седой", 40));
        for(Animal b: animals){
            if (b instanceof Turtle){
                System.out.println("\nКласс черепаха");
                b.printer();
            }
            else if (b instanceof Hamster){
                System.out.println("\nКласс хомяк");
                b.printer();
            }
            else {
                System.out.println("\nКласс животное");
                b.printer();
            }
        }
    }
}
