public class Turtle extends Animal {
    boolean liveInWater;

    public Turtle(String name, int age, String color, boolean liveInWater) {
        super(name, age, color);
        this.liveInWater = liveInWater;
    }

    @Override
    public void printer() {
        System.out.printf("\nName: %s\nAge: %d\nColor: %s\nLives in water: %b", name, age, color, liveInWater);
    }
}
