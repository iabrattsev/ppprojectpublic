package com.mpt.apsints.abstraction.ex003_abstraction;

/**
 * Абстрактный класс может быть унаследован от абстрактного класса.
 * Реализация абстрактного метода из базового абстрактного класса, в производном абстрактном классе - не обязательна.
 */

/**
 * Абстрактный класс A.
 */
abstract class AbstractClassA {
    public abstract void operationA();
}

/**
 * Абстрактный класс B.
 */
abstract class AbstractClassB extends AbstractClassA {
    public abstract void operationB();

}

/**
 * Конкретный класс.
 */
class ConcreteClass extends AbstractClassB {
    @Override
    public void operationA() {
        System.out.println("ConcreteClass.operationA");
    }

    @Override
    public void operationB() {
        System.out.println("ConcreteClass.operationB");
    }
}

public class Main {
    public static void main(String[] args) {
        AbstractClassA instance = new ConcreteClass();

        instance.operationA();

       // instance.operationB();  // Вопрос: почему недоступен данный метод?
    }
}
