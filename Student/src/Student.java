public class Student {
    public String FIO;
    public String group;
    public float performance;

    public Student(String FIO, String group, float performance){
        this.FIO = FIO;
        this.group = group;
        this.performance = performance;
    }

    public float getPerformance() {
        return performance;
    }

    public String getFIO() {
        return FIO;
    }

    public String getGroup() {
        return group;
    }

    public void printer(){
        System.out.printf("Инициалы и фамилия: %s\nГруппа: %s\nУспеваемость: %f\n", FIO, group, performance);
    }
}
