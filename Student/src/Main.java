import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Student[] stud = new Student[5];
        for(int i = 0; i <= 4; i++){
            stud[i] = new Student(inputZnach("Инициалы и фамилия: "), inputZnach("Группа: "), Float.valueOf(inputZnach("Успеваемость: ")));
        }
        Student temp;
        for (int i = 0; i <= 4; i++){
            for (int j = i + 1; j <= 4; j++){
                if (stud[i].performance > stud[j].performance){
                    temp = stud[i];
                    stud[i] = stud[j];
                    stud[j] = temp;
                }
            }
        }
        for (int i = 0; i <= 4; i++){
            stud[i].printer();
        }
        Scanner scn = new Scanner(System.in);
        System.out.println("Вывести учеников с оценками 4 и 5?\n[1-да 0-нет]");
        byte choice = 0;
        choice = scn.nextByte();
        switch (choice){
            case 1:{
                for (int i = 0; i <= 4; i++){
                    if (stud[i].performance >= 4) {
                        stud[i].printer();
                    }
                }
                break;
            }
            default:{
                break;
            }
        }
    }

    public static String inputZnach(String type){
        Scanner scanner = new Scanner(System.in);
        System.out.println(type);
        return scanner.next();
    }
}
