import java.util.ArrayList;
import java.util.List;

abstract class Figure{
    public abstract double Perimetr();

    public abstract double Square();
}

public class Main {
    public static void main(String[] args) {
        List<Figure> triangleList = new ArrayList<>();
        triangleList.add(new Triangle(10, 5, 6, 0, 0));
        triangleList.add(new Rectangle(15, 25, 0, 0));
        triangleList.add(new Circle(25, 0, 0));
        for(Figure fg : triangleList){
            if (fg instanceof Rectangle) {
                System.out.println("Прямоугольник");
                System.out.println(fg.toString());
            }
            else if (fg instanceof Triangle){
                System.out.println("Треугольник");
                System.out.println(fg.toString());
            }
            else if (fg instanceof Circle){
                System.out.println("Круг");
                System.out.println(fg.toString());
            }
            else
                System.out.println(fg.toString());
        }
    }
}
