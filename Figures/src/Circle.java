public class Circle extends Figure{
    public double r, p, s;

    public Circle(double r, double p, double s){
        this.r = r;
        this.p = p;
        this.s = s;
    }

    public double Perimetr(){
        p = 2*Math.PI*r;
        return p;
    }

    public double Square(){
        s = Math.PI * Math.pow(r, 2);
        return s;
    }

    public String toString(){
        return String.format("Радиус: %f\nПериметр: %f\nПлощадь: %f\n\n", r, Perimetr(), Square());
    }
}
