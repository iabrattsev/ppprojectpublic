public class Triangle extends Figure{
    public double a, b, c, p, s;

    public Triangle(double a, double b, double c, double p, double s){
        this.a = a;
        this.b = b;
        this.c = c;
        this.p = p;
        this.s = s;
    }

    @Override
    public double Perimetr(){
        p = a + b +c;
        return p;
    }

    public double Square(){
        s = Math.sqrt(Perimetr()*(Perimetr()-a)*(Perimetr()-b)*(Perimetr()-c));
        return s;
    }

    public String toString(){
        return String.format("Вершина А: %f\nВершина B: %f\nВершина С: %f\nПериметр: %f\nПлощадь: %f\n\n", a, b, c, Perimetr(), Square());
    }
}
