public class Rectangle extends Figure{
    double a, b, p, s;

    public Rectangle(double a, double b, double p, double s){
        this.a = a;
        this.b = b;
        this.p = p;
        this.s = s;
    }

    public double Perimetr(){
        p = a*2 + b*2;
        return p;
    }

    public double Square(){
        s = a*b;
        return s;
    }

    public String toString(){
        return String.format("Вершина А: %f\nВершина В: %f\nПериметр: %f\nПлощадь: %f\n\n", a, b, Perimetr(), Square());
    }
}
