public class Potato {
    private String country; //страна производителя
    private float size;
    private String color;
    private String type;
    private int speed;
    private int cost;
    private int id;

    public  Potato(String country, float size, String color, String type, int speed, int cost, int id){
        this.country = country;
        this.size = size;
        this.color = color;
        this.type = type;
        this.speed = speed;
        this.cost = cost;
        this.id = id;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public void setSize(float size) {
        this.size = size;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCountry() {
        return country;
    }

    public float getSize() {
        return size;
    }

    public String getColor() {
        return color;
    }

    public String getType() {
        return type;
    }

    public int getSpeed() {
        return speed;
    }

    public int getCost() {
        return cost;
    }

    public int getId() {
        return id;
    }

    @Override
    public String toString() {
        return String.format("Страна производителя: %s\nРазмер: %f\nЦвет: %s\nСорт: %s\nСкорость роста: %d\nЦена: %d\nID: %d",
                country, size, color, type, speed, cost, id);
    }
}
