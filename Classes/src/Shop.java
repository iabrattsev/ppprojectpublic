import java.util.Scanner;

public class Shop {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Чем торговать на рынке будете?\n1 - Картошка\n2 - Видеокарты");
        int choose = 0;
        choose = scanner.nextInt();
        switch (choose) {
            case 1:
                int c = 10;
                Potato[] potatoes = new Potato[c];
                int j = 0;
                while (true){
                    System.out.println("1 - Добавление товара\n2 - Редактировать товар\n3 - Просмотр товаров");
                    String check = scanner.next();
                    switch (check){
                        case "1":
                            System.out.println("Добавление товара");
                            potatoes[j++] = new Potato(inputZnach("Страна производителя: "), Float.valueOf(inputZnach("Размер(диаметр, см.): ")),
                                    inputZnach("Цвет: "), inputZnach("Сорт: "), Integer.valueOf(inputZnach("Скорость роста(дней): ")),
                                    Integer.valueOf(inputZnach("Цена: ")), j);
                            break;
                        case "2":
                            System.out.println("Редактировать товар");
                            int id1 = scanner.nextInt();
                            if (id1 < j) {
                                System.out.println(potatoes[id1]);
                                String vib1 = scanner.next();
//                                potatoes[id1] = new Potato(inputZnach("Страна производителя: "), Float.valueOf(inputZnach("Размер(диаметр, см.): ")),
//                                        inputZnach("Цвет: "), inputZnach("Сорт: "), Integer.valueOf(inputZnach("Скорость роста(дней): ")),
//                                        Integer.valueOf(inputZnach("Цена: ")), j);

                                while (!vib1.equals("=")) {
                                    switch (vib1) {
                                        case "1":
                                            potatoes[id1].setCountry(inputZnach("Редактирование страны"));
                                            break;
                                        case "2":
                                            potatoes[id1].setSize(Float.valueOf(inputZnach("Редактирование размера")));
                                            break;
                                        case "3":
                                            potatoes[id1].setColor(inputZnach("Редактирование цвета"));
                                            break;
                                        case "4":
                                            potatoes[id1].setType(inputZnach("Редактирование сорта"));
                                            break;
                                        case "5":
                                            potatoes[id1].setSpeed(Integer.valueOf(inputZnach("Редактирование скорости роста")));
                                            break;
                                        case "6":
                                            potatoes[id1].setCost(Integer.valueOf(inputZnach("Редактирование цены")));
                                            break;
                                        case "7":
                                            potatoes[id1].setId(Integer.valueOf(inputZnach("Редактирование ID")));
                                            default:
                                                break;

                                    }
                                    vib1 = scanner.next();

                                }
                                //vib1 = scanner.next();
                                break;
                            }
                        case "3":
                            System.out.println("Просмотр товара");
                            for (int f = 0; f < j; f++) {
                                System.out.println(potatoes[f]);
                            }
                            break;
                        default:
                            break;
                    }
                }

            case 2:
                int count = 10;
                VideoCards[] videoCards = new VideoCards[count];
                int i = 0;
                while (true) {
                    System.out.println("1 - Добавление товара\n2 - Редактировать товар\n3 - Просмотр товара");
                    String input = scanner.next();
                    switch (input) {
                        case "1":
                            System.out.println("Добавление товара");
                            videoCards[i++] = new VideoCards(inputZnach("Введите прозводителя: "),
                                    inputZnach("Введите название: "), Double.valueOf(inputZnach("Введите цену: ")),
                                    Integer.valueOf(inputZnach("Введите объём видеопамяти: ")),
                                    Float.valueOf(inputZnach("Введите частоту: ")));
                            break;
                        case "2":
                            System.out.println("Редактировать товар");
                            int id = scanner.nextInt();
                            if (id < i) {
                                System.out.println(videoCards[id]);
                                String vib = scanner.next();
                                videoCards[id] = new VideoCards(inputZnach("Введите прозводителя: "),
                                        inputZnach("Введите название: "), Double.valueOf(inputZnach("Введите цену: ")),
                                        Integer.valueOf(inputZnach("Введите объём видеопамяти: ")),
                                        Float.valueOf(inputZnach("Введите частоту: ")));
                                while (vib.equals("=")) {
                                    switch (vib) {
                                        case "1":
                                            videoCards[id].setDeveloper(inputZnach("Редактирование разработчка"));
                                            break;
                                        case "2":
                                            videoCards[id].setName(inputZnach("Редактирование названия"));
                                            break;
                                        case "3":
                                            videoCards[id].setCost(Double.valueOf(inputZnach("Редактирование цены")));
                                            break;
                                        case "4":
                                            videoCards[id].setVideoMemory(Integer.valueOf(inputZnach("Редактирование видеопамяти")));
                                            break;
                                        case "5":
                                            videoCards[id].setClock(Float.valueOf(inputZnach("Редактирование частоты")));
                                            break;

                                    }
                                }
                                vib = scanner.next();
                            }
                            break;
                        case "3":
                            System.out.println("Просмотр товара");
                            for (int k = 0; k < i; k++) {
                                System.out.println(videoCards[k]);
                            }
                            break;
                        default:
                            break;
                    }
                }
        }
    }

    public static String inputZnach(String type){
        Scanner scanner = new Scanner(System.in);
        System.out.println(type);
        return scanner.next();
    }
}
