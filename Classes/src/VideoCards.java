public class VideoCards {
    private String developer; //Производитель
    private int videoMemory;
    private float clock; //Частота
    private String name;
    private double cost;

    public VideoCards(String developer, String name, double cost, int videoMemory, float clock){
        this.developer = developer;
        this.name = name;
        this.cost = cost;
        this.videoMemory = videoMemory;
        this.clock = clock;
    }

    public void setDeveloper(String developer) {
        this.developer = developer;
    }

    public void setVideoMemory(int videoMemory) {
        this.videoMemory = videoMemory;
    }

    public void setClock(float clock) {
        this.clock = clock;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }

    public String getDeveloper() {
        return developer;
    }

    public int getVideoMemory() {
        return videoMemory;
    }

    public float getClock() {
        return clock;
    }

    public String getName() {
        return name;
    }

    public double getCost() {
        return cost;
    }

    @Override
    public String toString() {
        return String.format("Производитель: %s \nНазвание %s\nЦена: %f\nВидеопамять: %d\nЧастота: %f", developer, name, cost,
                videoMemory, clock);
    }
}
