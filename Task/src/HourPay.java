public class HourPay extends Payment{
    public String FIO;
    public double HourPay;
    public double Salary;

    public HourPay(String FIO, double HourPay, double Salary){
        this.FIO = FIO;
        this.HourPay = HourPay;
        this.Salary = Salary;
    }

    public double Counting(){
        Salary = 20.8 * 8 * HourPay;
        return Salary;
    }

    public double getSalary() {
        return Salary;
    }

    public String toString(){
        return String.format("ФИО: %s\nПочасовая оплата: %f\nЗаработная плата: %f\n", FIO, HourPay, Counting());
    }
}
