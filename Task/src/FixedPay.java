public class FixedPay extends Payment{
    public String FIO;
    public double Salary;

    public FixedPay(String FIO, double Salary){
        this.FIO = FIO;
        this.Salary = Salary;
    }

    public double Counting(){
        return Salary;
    }

    public double getSalary() {
        return Salary;
    }

    public String toString(){
        return String.format("ФИО: %s\nЗаработная плата: %f\n", FIO, Counting());
    }
}
