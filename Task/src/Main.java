abstract class Payment{
    public abstract double Counting();
}

public class Main {
    public static void main(String[] args) {
        //List<Payment> employees = new ArrayList<>();
        Payment[] employees = new Payment[10];
        //employees.add(new HourPay("Степанов Дмитрий Алексеевич", 150, 0));
        //employees.add(new FixedPay("Борзенков Артемий Валерьевич", 15000));
        employees[0] = new HourPay("Степанов Дмитрий Алексеевич", 150, 0);
        employees[1] = new FixedPay("Борзенков Артемий Валерьевич", 15000);
        employees[2] = new HourPay("Братцев Илья Алексеевич", 1000, 0);
        employees[3] = new FixedPay("Лапин Владимирович", 10000);
        employees[4] = new HourPay("Иванов Кирилл Романович", 150, 0);
        employees[5] = new FixedPay("Александров Любомир Святославович", 30000);
        employees[6] = new HourPay("Елисеев Максимилиан Васильевич", 100, 0);
        employees[7] = new FixedPay("Мельников Наум Иринеевич", 35000);
        employees[8] = new HourPay("Третьяков Кондратий Яковлевич", 50, 0);
        employees[9] = new FixedPay("Гришин Донат Федосеевич", 14000);

        Payment temp;
        for (int i = 0; i < 10; i++){
            for (int j = i + 1; j < 10; j++){
                if (employees[i].Counting() < employees[j].Counting()){
                    temp = employees[i];
                    employees[i] = employees[j];
                    employees[j] = temp;
                }
            }
        }
        System.out.println("Первые 5 работников: ");
        for(int i = 0; i < 5; i++){
            System.out.println(employees[i].toString());
        }
        System.out.println("Последние 3 работника: ");
        for(int i = 9; i > 6; i--){
            System.out.println(employees[i].toString());
        }
    }
}
